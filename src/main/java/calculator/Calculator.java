package calculator;

public class Calculator {

    public int plus(int a, int b){
        return a + b;
    }

    public static void main(String[] args){
        Calculator calc = new Calculator();

        if(args.length != 2){
            System.out.println("Wrong number of arguments");
            return;
        }
        int arg1 = Integer.parseInt(args[0]);
        int arg2 = Integer.parseInt(args[1]);

        System.out.println(calc.plus(arg1, arg2));

    }
}


