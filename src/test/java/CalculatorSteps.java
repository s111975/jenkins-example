import calculator.Calculator;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorSteps {
    Calculator calc;
    int result;

    @Given("I have a calculator")
    public void iHaveACalculator() {
        calc = new Calculator();
    }

    @When("I add {int} and {int}")
    public void iAddAnd(int a, int b) {
        result = calc.plus(a, b);
    }

    @Then("I get {int} as a result")
    public void iGetAsAResult(int r) {
        assertEquals(r, result);
    }
}
